/**************************
 *Mackenzie Blue
 *CPSC 1021, 003, F20
 *mblue@clemson.edu
 *Elliot McMillan and Victoria Xu
 **************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

//create employee struct
typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;
//define functions (name_order sorts names in alphabetical order, myrandom feeds random employees to be sorted)
bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	//create employee array and prompt user to enter info for 10 employees
	employee employees[10];
	for(int i = 0; i < 10; ++i){
		cout << "Please enter the employee first name: ";
		cin >> employees[i].firstName;
		cout << "Please enter the employee last name: ";
		cin >> employees[i].lastName;
		cout << "Enter the employee birth year: ";
		cin >> employees[i].birthYear;
		cout << "Enter the employee hourly wage: ";
		cin >> employees[i].hourlyWage;
	}


  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	 employee* begPointer = &employees[0];
	 employee* endPointer = &employees[10];
	 //shuffle the 10 employees with random_shuffle function (already implemented in C++)
	 random_shuffle(begPointer, endPointer, myrandom);


   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
		//fiveEmployees array is equal to the first 5 elements of employees array
		employee fiveEmployees[5] = {employees[0],employees[1],employees[2],employees[3],employees[4]};

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		 //pass first and last employees in list and sort them in order by name
		 sort(&fiveEmployees[0], &fiveEmployees[5], name_order);



    /*Now print the array below */
		//in a for loop, print all info for the 5 sorted employees
		for (int i = 0; i < 5; ++i){
			cout << setw(5) << fiveEmployees[i].lastName << ", " << setw(5) << fiveEmployees[i].firstName << endl;
			cout << fiveEmployees[i].birthYear << endl;
			cout << setprecision(2) << fiveEmployees[i].hourlyWage << endl;

		}



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
// return last names in order from lowest letter of alphabet to highest
bool name_order(const employee& lhs, const employee& rhs) { return lhs.lastName < rhs.lastName; }
  // IMPLEMENT
